package com.arazdev.mystry.model

class PostsModel {

    val postTable = "posts"
    val titleField = "title"
    val authorField = "author"
    val contentField = "content"
    val idField = "id"
    val dateField = "datestamp"

}