package com.arazdev.mystry.model

class CategoriesModel {

    val catTable = "categories"
    val sadField = "sad"
    val happyField = "happy"
    val mysteryField = "mystery"
    val adviceField = "advice"
    val angryField = "angry"

}