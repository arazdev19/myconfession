package com.arazdev.mystry.model

class PublicPostModel {

    val pubPostTable = "publicposts"
    val pubTitleField = "title"
    val pubAuthorField = "author"
    val pubContentField = "content"
    val pubIdField = "id"
    val pubDateField = "datestamp"
    val pubReadCounts = "readcounts"

}