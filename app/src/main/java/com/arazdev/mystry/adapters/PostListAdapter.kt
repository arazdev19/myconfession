package com.arazdev.mystry.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.arazdev.mystry.R
import com.arazdev.mystry.view.ReadPostActivity
import kotlinx.android.synthetic.main.row_post_lists.view.*
import org.json.JSONArray

class PostListAdapter(private val c:Context,private val postListArray:JSONArray):RecyclerView.Adapter<PostListAdapter.PostListViewHolder>() {

    inner class PostListViewHolder(view:View):RecyclerView.ViewHolder(view){
        internal var postcard = view.postcard
        internal var posttitle = view.posttitle
        internal var postcontent = view.postcontent
        internal var readlikecount = view.likecount
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_post_lists,parent,false)
        return PostListViewHolder(view)
    }

    override fun onBindViewHolder(holder: PostListViewHolder, position: Int) {
        val dataObject = postListArray[position] as HashMap<*, *>
        val author = dataObject["author"].toString()
        val title = dataObject["title"].toString()
        val content = dataObject["content"].toString()
        val id = dataObject["id"].toString()
        val datestamp = dataObject["datestamp"].toString()
        val readcount = dataObject["readcounts"].toString()
        val previewContent = content.replace("\n","")
        val contentLength = previewContent.length
        val concatContent = previewContent.subSequence(contentLength/3,contentLength)


        holder.posttitle.text = title
        holder.postcontent.text = "..$concatContent"
        holder.readlikecount.text = readcount
        holder.postcard.setOnClickListener {
            val intent = Intent(c,ReadPostActivity::class.java)
            intent.putExtra("author",author)
            intent.putExtra("title",title)
            intent.putExtra("content",content)
            intent.putExtra("datestamp",datestamp)
            intent.putExtra("readcounts",readcount)
            intent.putExtra("id",id)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            c.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return postListArray.length()
    }

}