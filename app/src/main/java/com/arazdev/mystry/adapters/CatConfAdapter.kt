package com.arazdev.mystry.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.arazdev.mystry.R
import com.arazdev.mystry.model.PublicPostModel
import com.arazdev.mystry.view.ReadPostActivity
import kotlinx.android.synthetic.main.row_cat_conf_layout.view.*
import org.json.JSONArray

class CatConfAdapter(private var c:Context,private var a:JSONArray):RecyclerView.Adapter<CatConfAdapter.CatConfHolder>() {

    val pubPostModel = PublicPostModel()
    val pubPostTable = pubPostModel.pubPostTable
    val pubTitleField = pubPostModel.pubTitleField
    val pubAuthorField = pubPostModel.pubAuthorField
    val pubContentField = pubPostModel.pubContentField
    val pubIdField = pubPostModel.pubIdField
    val pubDateField = pubPostModel.pubDateField
    val pubReadCounts = pubPostModel.pubReadCounts

    inner class CatConfHolder(view: View):RecyclerView.ViewHolder(view){
        internal var confpost = view.relpostid
        internal var conftitle = view.posttitle
        internal var confcontent = view.postcontent
        internal var confcount = view.likecount
        internal var confread = view.postcard
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CatConfHolder {
        val v = LayoutInflater.from(c).inflate(R.layout.row_cat_conf_layout,parent,false)
        return CatConfHolder(v)
    }

    override fun getItemCount(): Int {
        return a.length()
    }

    override fun onBindViewHolder(holder: CatConfHolder, position: Int) {
        val data = a[position] as HashMap<*,*>
        val previewContent = data[pubContentField].toString().replace("\n","")
        val contentLength = previewContent.length
        val concatContent = previewContent.subSequence(contentLength/3,contentLength)
        holder.conftitle.text = data[pubTitleField].toString()
        holder.confcount.text = data[pubReadCounts].toString()
        holder.confcontent.text = "..$concatContent"
        holder.confread.setOnClickListener {
            val intent = Intent(c, ReadPostActivity::class.java)
            intent.putExtra("author",data[pubAuthorField].toString())
            intent.putExtra("title",data[pubTitleField].toString())
            intent.putExtra("content",data[pubContentField].toString())
            intent.putExtra("datestamp",data[pubDateField].toString())
            intent.putExtra("readcounts",data[pubReadCounts].toString())
            intent.putExtra("id",data[pubIdField].toString())
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            c.startActivity(intent)
        }
    }



}