package com.arazdev.mystry.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.arazdev.mystry.R
import com.arazdev.mystry.model.CommentsModel
import kotlinx.android.synthetic.main.row_comments_layout.view.*
import org.json.JSONArray

class CommensAdapter(private var c:Context,private var a:JSONArray):RecyclerView.Adapter<CommensAdapter.CommentsViewHolder>() {

    val commentsModel = CommentsModel()
    val commentsTable = commentsModel.commentsTable
    val commentsUser = commentsModel.commentsUser
    val commentsContent = commentsModel.commentsContent

    inner class CommentsViewHolder(view:View):RecyclerView.ViewHolder(view){
        internal var user = view.commentuser
        internal var content = view.commentcontents
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentsViewHolder {
        val v = LayoutInflater.from(c).inflate(R.layout.row_comments_layout,parent,false)
        return CommentsViewHolder(v)
    }

    override fun getItemCount(): Int {
        return a.length()
    }

    override fun onBindViewHolder(holder: CommentsViewHolder, position: Int) {
        val data = a.getJSONObject(position)
        val commentuser = data[commentsUser].toString()
        val commentcontents = data[commentsContent].toString()

        holder.user.text = commentuser
        holder.content.text = commentcontents
    }
}