package com.arazdev.mystry.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.arazdev.mystry.R
import com.arazdev.mystry.model.PublicPostModel
import com.arazdev.mystry.view.ReadPostActivity
import kotlinx.android.synthetic.main.all_con_row_layout.view.*
import org.json.JSONArray

class AllConViewPagerAdapter(private var c:Context,private var a:JSONArray):PagerAdapter() {

    val pubModel = PublicPostModel()
    val pubPostTable = pubModel.pubPostTable
    val pubTitleField = pubModel.pubTitleField
    val pubAuthorField = pubModel.pubAuthorField
    val pubContentField = pubModel.pubContentField
    val pubIdField = pubModel.pubIdField
    val pubDateField = pubModel.pubDateField
    val pubReadCounts = pubModel.pubReadCounts

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return a.length()
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val v = LayoutInflater.from(c).inflate(R.layout.all_con_row_layout,container,false)
        val dataObject = a[position] as HashMap<*,*>
        val author = dataObject[pubAuthorField].toString()
        val title = dataObject[pubTitleField].toString()
        val content = dataObject[pubContentField].toString()
        val id = dataObject[pubIdField].toString()
        val datestamp = dataObject[pubDateField].toString()
        val readcount = dataObject[pubReadCounts].toString()
        val previewContent = content.replace("\n","")
        val contentLength = previewContent.length
        val concatContent = previewContent.subSequence(contentLength/3,contentLength)
        v.allrowtitle.text = "\"$title\""
        v.allconrel.setOnClickListener {
            val intent = Intent(c, ReadPostActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra(pubTitleField,title)
            intent.putExtra(pubAuthorField,author)
            intent.putExtra(pubContentField,content)
            intent.putExtra(pubDateField,datestamp)
            intent.putExtra(pubReadCounts,readcount)
            intent.putExtra(pubIdField,id)
            c.startActivity(intent)
        }
        container.addView(v,0)

        return v
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }
}