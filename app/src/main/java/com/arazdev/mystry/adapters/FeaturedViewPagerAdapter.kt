package com.arazdev.mystry.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.arazdev.mystry.R
import com.arazdev.mystry.model.PublicPostModel
import com.arazdev.mystry.view.ReadPostActivity
import kotlinx.android.synthetic.main.featured_card_row_layout.view.*
import org.json.JSONArray

class FeaturedViewPagerAdapter(private var c: Context,private var a:JSONArray):PagerAdapter() {

    val pubModel = PublicPostModel()
    val pubPostTable = pubModel.pubPostTable
    val pubTitleField = pubModel.pubTitleField
    val pubAuthorField = pubModel.pubAuthorField
    val pubContentField = pubModel.pubContentField
    val pubIdField = pubModel.pubIdField
    val pubDateField = pubModel.pubDateField
    val pubReadCounts = pubModel.pubReadCounts

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return if(a.length() >= 10) 10 else a.length()
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val v = LayoutInflater.from(c).inflate(R.layout.featured_card_row_layout,container,false)
        val data = a.getJSONObject(position)

        val title = data.getString(pubTitleField)
        val previewContent = data.getString(pubContentField).replace("\n","")
        val contentLength = previewContent.length
        val concatContent = previewContent.subSequence(contentLength/3,contentLength)

        v.featured_header.text = title
        v.featured_title.text = "..${concatContent.toString()}"
        v.featuredreadcount.text = "${data.getString(pubReadCounts)} reads"
        v.featuredcardid.setOnClickListener {
            val intent = Intent(c,ReadPostActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra(pubTitleField,data.getString(pubTitleField))
            intent.putExtra(pubAuthorField,data.getString(pubAuthorField))
            intent.putExtra(pubContentField,data.getString(pubContentField))
            intent.putExtra(pubDateField,data.getString(pubDateField))
            intent.putExtra(pubReadCounts,data.getString(pubReadCounts))
            intent.putExtra(pubIdField,data.getString(pubIdField))
            c.startActivity(intent)
        }

        container.addView(v,0)

        return v
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }
}