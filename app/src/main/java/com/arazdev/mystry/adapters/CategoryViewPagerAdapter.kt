package com.arazdev.mystry.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.arazdev.mystry.R
import com.arazdev.mystry.model.PublicPostModel
import com.arazdev.mystry.view.CatConfListActivity
import kotlinx.android.synthetic.main.category_card_row_layout.view.*
import org.json.JSONArray

class CategoryViewPagerAdapter(private var c:Context,private var a:JSONArray):PagerAdapter() {

    val pubPostModel = PublicPostModel()
    val pubPostTable = pubPostModel.pubPostTable
    val pubTitleField = pubPostModel.pubTitleField
    val pubAuthorField = pubPostModel.pubAuthorField
    val pubContentField = pubPostModel.pubContentField
    val pubIdField = pubPostModel.pubIdField
    val pubDateField = pubPostModel.pubDateField
    val pubReadCounts = pubPostModel.pubReadCounts

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return a.length()
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val v = LayoutInflater.from(c).inflate(R.layout.category_card_row_layout,container,false)

        val catObj = a.getJSONObject(position)
        val allIds = catObj.getString("confid")

        v.category_title.text = catObj.getString("name")
        v.catconcount.text = "${catObj.getString("count")} Confessions"

        v.catcardid.setOnClickListener {
            val intent = Intent(c,CatConfListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            intent.putExtra("id",allIds)
            c.startActivity(intent)
        }

        container.addView(v,0)

        return v
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }
}