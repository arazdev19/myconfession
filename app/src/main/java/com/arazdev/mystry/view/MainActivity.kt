package com.arazdev.mystry.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.arazdev.mystry.R
import com.arazdev.mystry.adapters.AllConViewPagerAdapter
import com.arazdev.mystry.adapters.CategoryViewPagerAdapter
import com.arazdev.mystry.adapters.FeaturedViewPagerAdapter
import com.arazdev.mystry.viewmodel.CategoriesViewModel
import com.arazdev.mystry.viewmodel.MainViewModel
import com.arazdev.mystry.viewmodel.WritePostViewModel
import kotlinx.android.synthetic.main.main_layout.*

class MainActivity:AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_layout)
        supportActionBar!!.hide()

        val wpvm = ViewModelProviders.of(this).get(WritePostViewModel::class.java)
        val cvm = ViewModelProviders.of(this).get(CategoriesViewModel::class.java)
        val mvm = ViewModelProviders.of(this).get(MainViewModel::class.java)
        
        wpvm.getConfessCount()
        mvm.getAllConfessions()
        mvm.mtballconfessions.observe(this, Observer {
            allconviewpager.adapter = AllConViewPagerAdapter(applicationContext,it)
            allconviewpager.setPadding(80,0,80,0)
            allconviewpager.addOnPageChangeListener(object:ViewPager.OnPageChangeListener{
                override fun onPageScrollStateChanged(state: Int) {

                }

                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

                }

                override fun onPageSelected(position: Int) {

                }

            })
        })

        wpvm.mtbpd.observe(this, Observer {
            if(it == "1"){
                mainprogressdialog.visibility = View.VISIBLE
            }

            if(it == "0"){
                mainprogressdialog.visibility = View.INVISIBLE
            }
        })

        writepostbtn.setOnClickListener {
            val intent = Intent(this, WritepostActivity::class.java)
            startActivity(intent)
        }

        unameid.setOnClickListener {
            val intent = Intent(this, PostListsActivity::class.java)
            startActivity(intent)
        }

        cvm.getCount()
        cvm.mtbcatloading.observe(this, Observer {
            if(it == "1"){
                catpd.visibility = View.VISIBLE
            }

            if(it == "0"){
                catpd.visibility = View.INVISIBLE
            }
        })

        mvm.mtbfeatureloading.observe(this, Observer {
            if(it == "1"){
                featuredpd.visibility = View.VISIBLE
            }

            if(it == "0"){
                featuredpd.visibility = View.INVISIBLE
            }
        })

        cvm.mtbcatcount.observe(this, Observer {
            categoryviewpager.adapter = CategoryViewPagerAdapter(applicationContext,it)
            categoryviewpager.setPadding(80,0,80,0)
            categoryviewpager.addOnPageChangeListener(object:ViewPager.OnPageChangeListener{
                override fun onPageScrollStateChanged(state: Int) {

                }

                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

                }

                override fun onPageSelected(position: Int) {

                }

            })
        })

        mvm.getFeaturedConfessions()
        mvm.mtbfeaturedarray.observe(this, Observer {
            featuredviewpager.adapter = FeaturedViewPagerAdapter(applicationContext,it)
            featuredviewpager.setPadding(80,0,80,0)
            featuredviewpager.addOnPageChangeListener(object:ViewPager.OnPageChangeListener{
                override fun onPageScrollStateChanged(state: Int) {

                }

                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

                }

                override fun onPageSelected(position: Int) {

                }

            })
        })

    }

}