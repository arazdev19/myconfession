package com.arazdev.mystry.view

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.arazdev.mystry.R
import com.arazdev.mystry.adapters.CatConfAdapter
import com.arazdev.mystry.adapters.PostListAdapter
import com.arazdev.mystry.viewmodel.CatConfViewModel
import kotlinx.android.synthetic.main.cant_conf_layout.*
import org.json.JSONArray

class CatConfListActivity:AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.cant_conf_layout)
        supportActionBar!!.hide()

        val catId = intent.getStringExtra("id")

        val ccvm = ViewModelProviders.of(this).get(CatConfViewModel::class.java)

        ccvm.getCatConf(catId)
        ccvm.mtbcatjsonarray.observe(this, Observer {
            val catconfadapter = CatConfAdapter(applicationContext,it)
            val layoutmanager = LinearLayoutManager(applicationContext)
            catconflistrecycler.layoutManager = layoutmanager
            catconflistrecycler.adapter = catconfadapter
            catconfadapter.notifyDataSetChanged()

            catlistsearch.setOnQueryTextListener(object:SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    return if(newText == ""){
                        ccvm.getCatConf(catId)
                        true
                    }else{
                        val dataSearch = JSONArray()
                        for(i in 0 until it.length()){
                            val d = it[i] as HashMap<*,*>
                            if(d["author"].toString().toUpperCase().contains(newText.toString().toUpperCase()) || d["title"].toString().toUpperCase().contains(newText.toString().toUpperCase())){
                                dataSearch.put(d)
                            }
                        }

                        val catconfadaptersearch = PostListAdapter(applicationContext,dataSearch)
                        val layoutManagersearch = LinearLayoutManager(applicationContext)
                        catconflistrecycler.layoutManager = layoutManagersearch
                        catconflistrecycler.adapter = catconfadaptersearch
                        catconfadapter.notifyDataSetChanged()
                        true
                    }
                }

            })
        })

        ccvm.mtbconfpd.observe(this, Observer {
            if(it == "1"){
                contconfpd.visibility = View.VISIBLE
            }

            if(it == "0"){
                contconfpd.visibility = View.INVISIBLE
            }
        })
    }
}