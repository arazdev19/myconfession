package com.arazdev.mystry.view

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.arazdev.mystry.R
import com.arazdev.mystry.adapters.CommensAdapter
import com.arazdev.mystry.viewmodel.CommentViewModel
import com.arazdev.mystry.viewmodel.ReadPostViewModel
import com.facebook.share.model.*
import com.facebook.share.widget.ShareDialog
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.formats.NativeAdOptions
import com.google.android.gms.ads.formats.NativeAdOptions.ADCHOICES_TOP_LEFT
import com.google.android.gms.ads.formats.UnifiedNativeAdView
import kotlinx.android.synthetic.main.custom_comment_alert_dialog.*
import kotlinx.android.synthetic.main.read_post_layout.*
import kotlinx.android.synthetic.main.unified_ads_layout.view.*
import kotlinx.android.synthetic.main.unified_ads_layout.view.adsimage
import java.net.URI
import java.net.URL


class ReadPostActivity:AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.read_post_layout)
        supportActionBar!!.hide()

        var title: String
        var author:String
        var content:String
        var datestamp:String
        var readcount:String
        var id:String

        val appLinkIntent = intent
        val appLinkData = appLinkIntent.data
        val rpvm = ViewModelProviders.of(this).get(ReadPostViewModel::class.java)

        MobileAds.initialize(applicationContext)
        val adLoader = AdLoader.Builder(applicationContext,resources.getString(R.string.admobprod))
            .forUnifiedNativeAd {
                if(it != null){
                    val adView = layoutInflater.inflate(R.layout.unified_ads_layout,null) as UnifiedNativeAdView
                    val adNatImgList = it.icon

                    adView.adstitle.text = it.headline
                    adView.headlineView = adView.adstitle

                    adView.adscontent.text = it.body
                    adView.bodyView = adView.adscontent

                    if(adNatImgList != null){
                        val adNatImg = adNatImgList.drawable
                        adView.adsimage.setImageDrawable(adNatImg)
                        adView.iconView = adView.adsimage
                    }

                    adView.adsadvertisers.text = it.advertiser
                    adView.advertiserView = adView.adsadvertisers

                    adView.setNativeAd(it)
                    adframe.removeAllViews()
                    adframe.addView(adView)
                }
            }
            .withAdListener(object: AdListener(){

            }).withNativeAdOptions(NativeAdOptions.Builder().setAdChoicesPlacement(
                ADCHOICES_TOP_LEFT
            ).build()).build()


        adLoader.loadAd(AdRequest.Builder().build())


        if(appLinkData != null){
            val pid = appLinkData.getQueryParameter("id")
            rpvm.getAppLinksConf(pid)
            rpvm.mtbapplinkres.observe(this, Observer {
                title = it.getString("title")
                author = it.getString("author")
                content = it.getString("content")
                datestamp = it.getString("datestamp")
                readcount = it.getString("readcounts")
                id = it.getString("id")

                showConfe(title,author,content,datestamp,readcount,id, rpvm)
            })

        }else{
            title = intent.getStringExtra("title")
            author = intent.getStringExtra("author")
            content = intent.getStringExtra("content")
            datestamp = intent.getStringExtra("datestamp")
            readcount = intent.getStringExtra("readcounts")
            id = intent.getStringExtra("id")

            showConfe(title,author,content,datestamp,readcount,id, rpvm)
        }
    }

    fun showConfe(title:String,author:String,content:String,datestamp:String,readcount:String,id:String,rpvm:ReadPostViewModel){
        var scrolldetect = "1"

        titleread.text = title
        contentread.text = content
        authorread.text = "By: $author"
        posttimestamp.text = datestamp
        readlikecount.text = readcount

        val previewContent = content.replace("\n","")
        val contentLength = previewContent.length
        val concatContent = previewContent.subSequence(contentLength/3,contentLength)

        readscrollid.viewTreeObserver.addOnGlobalLayoutListener {
            if(relinnerid.height <= readscrollid.height){
                rpvm.readPosts(id)
            }else{
                readscrollid.viewTreeObserver.addOnScrollChangedListener {
                    if(readscrollid != null){
                        if(readscrollid.getChildAt(0).bottom <= (readscrollid.height) + readscrollid.scrollY){
                            if(scrolldetect == "1"){
                                rpvm.readPosts(id)
                                scrolldetect = "0"
                            }
                        }
                    }
                }
            }
        }

        val cvm = ViewModelProviders.of(this).get(CommentViewModel::class.java)
        cvm.getReadCommentsCount(id)
        cvm.mtbreadcommentscount.observe(this, Observer {
            readcommcount.text = "$it"
        })
        commentbtn.setOnClickListener {
            val commentdialog = Dialog(this,android.R.style.Theme_Light_NoTitleBar_Fullscreen)
            commentdialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            commentdialog.setContentView(R.layout.custom_comment_alert_dialog)
            val commedittext = commentdialog.commentedttext
            val commnickname = commentdialog.commentsnickname
            val commsubmit = commentdialog.commentsubmitbtn
            val commcounts = commentdialog.commentcounts

            cvm.getComments(id)
            cvm.mtbcommentsjsonarray.observe(this, Observer {
                val commentsAdapter = CommensAdapter(applicationContext,it)
                val comlayoutman = LinearLayoutManager(applicationContext)
                commentdialog.commentrecycler.layoutManager = comlayoutman
                commentdialog.commentrecycler.adapter = commentsAdapter
                commentsAdapter.notifyDataSetChanged()
            })

            cvm.getCommentsCount(id)
            cvm.mtbcommentscount.observe(this, Observer {
                commcounts.text = if(it == "0"){
                    "No Comment"
                }else{
                    "$it Comments"
                }
            })

            cvm.mtbcommsuccess.observe(this, Observer {
                cvm.getComments(id)
            })

            commsubmit.setOnClickListener {
                if(!TextUtils.isEmpty(commedittext.text.toString()) && !TextUtils.isEmpty(commnickname.text.toString())){
                    cvm.PostComments(id,commedittext.text.toString(),commnickname.text.toString())
                    commedittext.text.clear()
                    commnickname.text.clear()
                }
            }
            commentdialog.show()
        }

        val shareDialog = ShareDialog(this)
        sharebtn.setOnClickListener {
            if(ShareDialog.canShow(ShareLinkContent::class.java)){
                val shareContent = ShareLinkContent.Builder()
                    .setQuote("..$concatContent..")
                    .setContentUrl(Uri.parse("https://mystry-75389.firebaseapp.com?id=$id"))
                    .build()

                shareDialog.show(shareContent)
            }
        }

    }


}