package com.arazdev.mystry.view

import android.app.Dialog
import android.app.ProgressDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.arazdev.mystry.R
import com.arazdev.mystry.viewmodel.WritePostViewModel
import kotlinx.android.synthetic.main.custom_alert_dialog.*
import kotlinx.android.synthetic.main.writepost_layout.*

class WritepostActivity:AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.writepost_layout)
        supportActionBar!!.hide()
        val pd = ProgressDialog(this)
        val wpvm = ViewModelProviders.of(this).get(WritePostViewModel::class.java)

        val loadingDialog = Dialog(this)
        loadingDialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        loadingDialog.setContentView(R.layout.custom_loading_layout)

        wpvm.mtbsuccess.observe(this, Observer {
            val customDialog = Dialog(this)
            customDialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            customDialog.setContentView(R.layout.custom_alert_dialog)
            customDialog.msgid.text = it
            customDialog.dialog_close_btn.setOnClickListener {
                customDialog.dismiss()
            }
            customDialog.show()

            customDialog.setOnDismissListener {
                finish()
            }
        })

        wpvm.mtberror.observe(this, Observer {
            Toast.makeText(applicationContext,it,Toast.LENGTH_SHORT).show()
        })

        wpvm.mtbpostloading.observe(this, Observer {
            if(it == "1"){
                loadingDialog.show()
            }

            if(it == "0"){
                loadingDialog.dismiss()
            }
        })

        sendpostbtn.setOnClickListener {
            if(!TextUtils.isEmpty(authorid.text) && !TextUtils.isEmpty(titleid.text) && !TextUtils.isEmpty(postid.text)){
                wpvm.postConfession(titleid.text.toString(),authorid.text.toString(),postid.text.toString())
            }else{
                Toast.makeText(applicationContext,"Please fill in all the fields",Toast.LENGTH_SHORT).show()
            }
        }
    }
}