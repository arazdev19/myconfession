package com.arazdev.mystry.view

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.arazdev.mystry.R
import com.arazdev.mystry.adapters.PostListAdapter
import com.arazdev.mystry.viewmodel.PostListViewModel
import kotlinx.android.synthetic.main.post_list_layout.*
import org.json.JSONArray

class PostListsActivity:AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.post_list_layout)
        supportActionBar!!.hide()

        val plvm = ViewModelProviders.of(this).get(PostListViewModel::class.java)
        plvm.getPostLists()

        plvm.mtblistresult.observe(this, Observer {
            val postrecycleradapter = PostListAdapter(applicationContext,it)
            val layoutManager = LinearLayoutManager(applicationContext)
            postlistrecycler.layoutManager = layoutManager
            postlistrecycler.adapter = postrecycleradapter
            postrecycleradapter.notifyDataSetChanged()

            postlistsearch.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    return if(newText == ""){
                        plvm.getPostLists()
                        true
                    }else{
                        val dataSearch = JSONArray()
                        for(i in 0 until it.length()){
                            val d = it[i] as HashMap<*,*>
                            if(d["author"].toString().toUpperCase().contains(newText.toString().toUpperCase()) || d["title"].toString().toUpperCase().contains(newText.toString().toUpperCase())){
                                dataSearch.put(d)
                            }
                        }

                        val postrecycleradaptersearch = PostListAdapter(applicationContext,dataSearch)
                        val layoutManagersearch = LinearLayoutManager(applicationContext)
                        postlistrecycler.layoutManager = layoutManagersearch
                        postlistrecycler.adapter = postrecycleradaptersearch
                        postrecycleradapter.notifyDataSetChanged()

                        //Toast.makeText(applicationContext,dataSearch.length().toString(),Toast.LENGTH_SHORT).show()
                        true
                    }

                }


            })
        })

        plvm.mtbpostlistpd.observe(this, Observer {
            if(it == "1"){
                postlistpd.visibility = View.VISIBLE
            }

            if(it == "0"){
                postlistpd.visibility = View.INVISIBLE
            }
        })


    }

}