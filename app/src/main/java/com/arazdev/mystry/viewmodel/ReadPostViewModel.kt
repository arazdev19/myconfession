package com.arazdev.mystry.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.arazdev.mystry.model.PublicPostModel
import com.google.firebase.firestore.FirebaseFirestore
import org.json.JSONObject

class ReadPostViewModel:ViewModel() {

    val pubPostModel = PublicPostModel()
    val pubPostTable = pubPostModel.pubPostTable
    val pubTitleField = pubPostModel.pubTitleField
    val pubAuthorField = pubPostModel.pubAuthorField
    val pubContentField = pubPostModel.pubContentField
    val pubIdField = pubPostModel.pubIdField
    val pubDateField = pubPostModel.pubDateField
    val pubReadCounts = pubPostModel.pubReadCounts

    val pubPostDb = FirebaseFirestore.getInstance()
    val pubPostRef = pubPostDb.collection(pubPostTable)

    val mtbreadresult = MutableLiveData<String>()
    val mtbapplinkres = MutableLiveData<JSONObject>()

    fun readPosts(id:String){

        val updateHash = HashMap<String,Any>()

        pubPostRef.document(id).get().addOnSuccessListener {
            val readCountsField = it.get(pubReadCounts).toString().toInt()
            val newReadCounts = readCountsField + 1

            updateHash[pubReadCounts] = newReadCounts
            pubPostRef.document(id).update(updateHash.toMap())
            mtbreadresult.value = "1"
        }

    }

    fun getAppLinksConf(id:String){
        val appLinkRed = JSONObject()
        pubPostRef.document(id).get().addOnSuccessListener {
            appLinkRed.put(pubTitleField,it.get(pubTitleField).toString())
            appLinkRed.put(pubAuthorField,it.get(pubAuthorField).toString())
            appLinkRed.put(pubContentField,it.get(pubContentField).toString())
            appLinkRed.put(pubIdField,it.get(pubIdField).toString())
            appLinkRed.put(pubDateField,it.get(pubDateField).toString())
            appLinkRed.put(pubReadCounts,it.get(pubReadCounts).toString())

            mtbapplinkres.value = appLinkRed
        }
    }

}