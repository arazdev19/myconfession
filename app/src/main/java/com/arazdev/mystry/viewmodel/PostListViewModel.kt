package com.arazdev.mystry.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.arazdev.mystry.model.PublicPostModel
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import org.json.JSONArray

class PostListViewModel:ViewModel() {

    // Posts Table
    val postPublicModel = PublicPostModel()
    val postPublicTable = postPublicModel.pubPostTable
    val pubTitleField = postPublicModel.pubTitleField
    val pubAuthorField = postPublicModel.pubAuthorField
    val pubContentField = postPublicModel.pubContentField
    val pubIdField = postPublicModel.pubIdField
    val pubDateField = postPublicModel.pubDateField
    val pubReadCounts = postPublicModel.pubReadCounts
    // Posts Table

    val db = FirebaseFirestore.getInstance()
    val postRef = db.collection(postPublicTable)

    val mtblistresult = MutableLiveData<JSONArray>()
    val mtberror = MutableLiveData<String>()
    val mtbpostlistpd = MutableLiveData<String>()

    fun getPostLists(){
        mtbpostlistpd.value = "1"
        postRef.orderBy(pubDateField,Query.Direction.DESCENDING).get().addOnSuccessListener {
            val dataJ = JSONArray()
            it.forEach { data ->
                dataJ.put(data.data)
            }
            mtblistresult.value = dataJ
            mtbpostlistpd.value = "0"
        }
    }
}