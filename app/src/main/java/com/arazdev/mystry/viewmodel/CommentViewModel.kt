package com.arazdev.mystry.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.arazdev.mystry.model.CommentsModel
import com.google.firebase.firestore.FirebaseFirestore
import org.json.JSONArray
import org.json.JSONObject

class CommentViewModel: ViewModel() {

    val commentsModel = CommentsModel()
    val commentsTable = commentsModel.commentsTable
    val commentsUser = commentsModel.commentsUser
    val commentsContent = commentsModel.commentsContent

    val db = FirebaseFirestore.getInstance()
    val comRef = db.collection(commentsTable)

    val mtbcommsuccess = MutableLiveData<String>()
    val mtbcommentsjsonarray = MutableLiveData<JSONArray>()
    val mtbcommentscount = MutableLiveData<String>()
    val mtbreadcommentscount = MutableLiveData<String>()

    fun PostComments(id:String,comments:String,nickname:String){
        val commentsHashMap = HashMap<Any,Any>()
        commentsHashMap[commentsContent] = comments
        commentsHashMap[commentsUser] = nickname

        comRef.document(id).collection(comRef.id).add(commentsHashMap).addOnCompleteListener {
            mtbcommsuccess.value = "1"
        }
    }

    fun getComments(id:String){
        val commentsJsonArray = JSONArray()
        comRef.document(id).collection("comments").get().addOnSuccessListener {
            it.forEach {data->
                val commJObject = JSONObject()
                commJObject.put(commentsUser,data.get(commentsUser))
                commJObject.put(commentsContent,data.get(commentsContent))
                commentsJsonArray.put(commJObject)
            }

            mtbcommentsjsonarray.value = commentsJsonArray
        }
    }

    fun getCommentsCount(id:String){
        comRef.document(id).collection("comments").get().addOnSuccessListener {
            mtbcommentscount.value = it.count().toString()
        }
    }

    fun getReadCommentsCount(id:String){
        comRef.document(id).collection("comments").get().addOnSuccessListener {
            mtbreadcommentscount.value = it.count().toString()
        }
    }

}