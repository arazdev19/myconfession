package com.arazdev.mystry.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.arazdev.mystry.model.PublicPostModel
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import org.json.JSONArray
import org.json.JSONObject

class MainViewModel:ViewModel() {

    val pubModel = PublicPostModel()
    val pubPostTable = pubModel.pubPostTable
    val pubTitleField = pubModel.pubTitleField
    val pubAuthorField = pubModel.pubAuthorField
    val pubContentField = pubModel.pubContentField
    val pubIdField = pubModel.pubIdField
    val pubDateField = pubModel.pubDateField
    val pubReadCounts = pubModel.pubReadCounts

    val pubDb = FirebaseFirestore.getInstance()
    val pubRef = pubDb.collection(pubPostTable)

    val mtbfeaturedarray = MutableLiveData<JSONArray>()
    val mtbfeatureloading = MutableLiveData<String>()
    val mtballconfessions = MutableLiveData<JSONArray>()


    fun getFeaturedConfessions(){
        mtbfeatureloading.value = "1"
        pubRef.orderBy(pubReadCounts,Query.Direction.DESCENDING).limit(5).get().addOnSuccessListener {
            val dataA = JSONArray()
            it.forEach {data->
                val dataJ = JSONObject()
                dataJ.put(pubIdField,data.id)
                dataJ.put(pubReadCounts,data.get(pubReadCounts))
                dataJ.put(pubTitleField,data.get(pubTitleField))
                dataJ.put(pubAuthorField,data.get(pubAuthorField))
                dataJ.put(pubContentField,data.get(pubContentField))
                dataJ.put(pubDateField,data.get(pubDateField))
                dataA.put(dataJ)
            }
            mtbfeaturedarray.value = dataA
            mtbfeatureloading.value = "0"
        }
    }

    fun getAllConfessions(){
        pubRef.orderBy(pubDateField,Query.Direction.DESCENDING).get().addOnSuccessListener {
            val dataJ = JSONArray()

            it.forEach {data->
                dataJ.put(data.data)
            }
            mtballconfessions.value = dataJ
        }
    }
}