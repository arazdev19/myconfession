package com.arazdev.mystry.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.arazdev.mystry.model.PublicPostModel
import com.google.firebase.firestore.FirebaseFirestore
import org.json.JSONArray

class CatConfViewModel:ViewModel() {

    val pubPostModel = PublicPostModel()
    val pubPostTable = pubPostModel.pubPostTable
    val pubTitleField = pubPostModel.pubTitleField
    val pubAuthorField = pubPostModel.pubAuthorField
    val pubContentField = pubPostModel.pubContentField
    val pubIdField = pubPostModel.pubIdField
    val pubDateField = pubPostModel.pubDateField
    val pubReadCounts = pubPostModel.pubReadCounts

    val pubPostDb = FirebaseFirestore.getInstance()
    val pubPostRef = pubPostDb.collection(pubPostTable)

    val mtbcatjsonarray = MutableLiveData<JSONArray>()
    val mtbconfpd = MutableLiveData<String>()

    fun getCatConf(catId:String){
        mtbconfpd.value = "1"
        val catJSon = JSONArray(catId)
        val resultArray = JSONArray()

        for (i in 0 until catJSon.length()){
            pubPostRef.whereEqualTo("id",catJSon[i]).get().addOnSuccessListener {
                it.forEach {data->
                    resultArray.put(data.data)
                }
                mtbconfpd.value = "0"
                mtbcatjsonarray.value = resultArray
            }
        }



    }

}