package com.arazdev.mystry.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.arazdev.mystry.model.PostsModel
import com.arazdev.mystry.model.PublicPostModel
import com.google.firebase.firestore.FirebaseFirestore
import java.text.SimpleDateFormat
import java.util.*

class WritePostViewModel:ViewModel() {

    // Posts Table
    val postModel = PostsModel()
    val postTable = postModel.postTable
    val titleField = postModel.titleField
    val authorField = postModel.authorField
    val contentField = postModel.contentField
    val idField = postModel.idField
    val dateField = postModel.dateField
    // Posts Table


    // Public Post Table
    val publicpostmodel = PublicPostModel()
    val publicPostTable = publicpostmodel.pubPostTable
    // Public Post Count

    val db = FirebaseFirestore.getInstance()


    val publicPostRef = db.collection(publicPostTable)

    val mtbsuccess = MutableLiveData<String>()
    val mtberror = MutableLiveData<String>()
    val mtbconfesscount = MutableLiveData<String>()
    val mtbpd = MutableLiveData<String>()
    val mtbpostloading = MutableLiveData<String>()



    fun postConfession(title:String,author:String,content:String){
        mtbpostloading.value = "1"
        val postRef = db.collection(postTable)
        val postHashtable = Hashtable<String,String>()
        val postId = postRef.document().id
        val date = SimpleDateFormat("dd-MM-yyyy",Locale.getDefault()).format(Date())

        postHashtable[titleField] = title
        postHashtable[authorField] = author
        postHashtable[contentField] = content
        postHashtable[idField] = postId
        postHashtable[dateField] = date

        postRef.document(postId).set(postHashtable.toMap()).addOnCompleteListener {
            if(it.isSuccessful){
                mtbsuccess.value = "Thank your for posting! We will verify your confession."
            }else{
                mtberror.value = "Opps! Something is wrong. Please try again later."
            }
            mtbpostloading.value = "0"
        }

    }

    fun getConfessCount(){
        mtbpd.value = "1"

        publicPostRef.get().addOnSuccessListener {
            mtbconfesscount.value = it.count().toString()
            mtbpd.value = "0"
        }

    }


}