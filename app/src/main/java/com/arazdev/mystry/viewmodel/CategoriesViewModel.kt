package com.arazdev.mystry.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.arazdev.mystry.model.CategoriesModel
import com.google.firebase.firestore.FirebaseFirestore
import org.json.JSONArray
import org.json.JSONObject

class CategoriesViewModel:ViewModel() {

    val catModel = CategoriesModel()
    val catTable = catModel.catTable
    val db = FirebaseFirestore.getInstance()
    val ref = db.collection(catModel.catTable)
    val mtbcatcount = MutableLiveData<JSONArray>()
    val mtbcatloading = MutableLiveData<String>()

    fun getCount(){
        mtbcatloading.value = "1"
        ref.get().addOnSuccessListener {
            val dataA = JSONArray()
            it.forEach {data->
                val dataJ = JSONObject()
                dataJ.put("name",data.id)
                dataJ.put("count",data.data.count())
                dataJ.put("confid",data.data.keys)
                dataA.put(dataJ)
            }

            mtbcatcount.value = dataA
            mtbcatloading.value = "0"
        }
    }

}